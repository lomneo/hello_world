FROM tomcat

RUN apt-get update && apt-get -y upgrade && apt-get -y install vim mc net-tools && mv webapps webapps2 && mv webapps.dist/ webapps

COPY index.html webapps/ROOT/

EXPOSE 8080
